const merge = require('webpack-merge'); // eslint-disable-line
const common = require('./webpack.common.js');

module.exports = merge(common, {
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    historyApiFallback: true,
    proxy: {
      '/api': 'http://localhost:8081',
      '/images': 'http://localhost:8081',
    },
  },
});
