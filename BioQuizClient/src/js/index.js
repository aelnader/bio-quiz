import "regenerator-runtime/runtime"; // eslint-disable-line
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

import css from '../css/bio-quiz.css'; // eslint-disable-line
import App from './components/container/App';
import mainSaga from './sagas/MainSaga';
import bioQuizzesApp from './reducers';

const sagaMiddleware = createSagaMiddleware();
/* eslint-disable no-underscore-dangle */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  bioQuizzesApp,
  composeEnhancers(applyMiddleware(sagaMiddleware)),
);
/* eslint-enable */
sagaMiddleware.run(mainSaga);

render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('app'),
);
