import quizzes from '../quizzes';
import * as actionTypes from '../../constants/actionTypes';

describe('quizzes reducer', () => {
  it('should return the initial state', () => {
    expect(quizzes(undefined, {})).toEqual({
      quizzesList: [],
      fetchingQuizzes: false,
      error: {},
    });
  });
  it('should handle QUIZZES_REQUESTED', () => {
    const state = {
      quizzesList: [],
      fetchingQuizzes: false,
      error: {},
    };
    expect(quizzes(state, {
      type: actionTypes.QUIZZES_REQUESTED,
    })).toEqual({
      quizzesList: [],
      fetchingQuizzes: true,
      error: {},
    });
  });
  it('should handle QUIZZES_SUCCEEDED', () => {
    const state = {
      quizzesList: [],
      fetchingQuizzes: true,
      error: {},
    };
    expect(quizzes(state, {
      type: actionTypes.QUIZZES_SUCCEEDED,
      payload: [
        {
          id: 1,
          title: 'Reproduction conforme de l’ADN',
          description: 'Ceci est descriptif génial du test',
          img: '/images/adn.jpg',
          questions: [{}, {}],
        },
      ],
    })).toEqual({
      quizzesList: [
        {
          id: 1,
          title: 'Reproduction conforme de l’ADN',
          description: 'Ceci est descriptif génial du test',
          img: '/images/adn.jpg',
          questions: [{}, {}],
        },
      ],
      fetchingQuizzes: false,
      error: {},
    });
  });
  it('should handle QUIZZES_FAILED', () => {
    const state = {
      quizzesList: [],
      fetchingQuizzes: true,
      error: {},
    };
    expect(quizzes(state, {
      type: actionTypes.QUIZZES_FAILED,
      payload: {
        code: '404',
        message: 'Page not found',
      },
    })).toEqual({
      quizzesList: [],
      fetchingQuizzes: false,
      error: {
        code: '404',
        message: 'Page not found',
      },
    });
  });
});
