import { combineReducers } from 'redux';
import quizzes from './quizzes';
import results from './results';

const bioQuizzesApp = combineReducers({
  quizzes,
  results,
});

export default bioQuizzesApp;
