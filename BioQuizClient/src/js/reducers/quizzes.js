import { QUIZZES_REQUESTED, QUIZZES_SUCCEEDED, QUIZZES_FAILED } from '../constants/actionTypes';
/**
 * Handles quizzes actions
 */
const initialState = {
  quizzesList: [],
  fetchingQuizzes: false,
  error: {},
};
const quizzes = (state = initialState, action) => {
  switch (action.type) {
    case QUIZZES_REQUESTED:
      return Object.assign({}, state, {
        fetchingQuizzes: true,
      });
    case QUIZZES_SUCCEEDED:
      return {
        fetchingQuizzes: false,
        error: {},
        quizzesList: action.payload,
      };
    case QUIZZES_FAILED:
      return Object.assign({}, state, {
        fetchingQuizzes: false,
        error: action.payload,
      });
    default:
      return state;
  }
};

export default quizzes;
