import { RESULTS_REQUESTED, RESULTS_SUCCEEDED, RESULTS_FAILED } from '../constants/actionTypes';
/**
 * Handles results actions
 */
const initialState = {
  resultsList: [],
  fetchingResults: false,
  error: {},
};
const results = (state = initialState, action) => {
  switch (action.type) {
    case RESULTS_REQUESTED:
      return Object.assign({}, state, {
        fetchingResults: true,
      });
    case RESULTS_SUCCEEDED:
      return {
        fetchingResults: false,
        error: {},
        resultsList: action.payload,
      };
    case RESULTS_FAILED:
      return Object.assign({}, state, {
        fetchingResults: false,
        error: action.payload,
      });
    default:
      return state;
  }
};

export default results;
