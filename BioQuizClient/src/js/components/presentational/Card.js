import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';
// constants
import { SELECT_QUIZ_BUTTON } from '../../constants/wordings';
import { QUIZZES_ROUTE } from '../../constants/routes';

/**
 * Component for showcasing a quiz
 * @param {string} title        quiz title
 * @param {string} description  quiz description
 * @param {string} pic          quiz cover url
 * @param {number} id           quiz id
 * @param {func} onQuizSelect   function to call when quiz is selected
 */
const Card = ({
  id,
  title,
  description,
  pic,
}) => {
  const divStylePic = {
    color: 'white',
    background: `url('${pic}')`,
    backgroundSize: '100%',
  };
  const divStyleMesures = {
    width: '320px',
    height: '320px',
  };
  return (
    <div className="card-parent">
      <div className="demo-card-square mdl-card mdl-shadow--2dp card" style={divStyleMesures}>
        <div className="mdl-card__title mdl-card--expand" style={divStylePic}>
          <h2 className="mdl-card__title-text">{title}</h2>
        </div>
        <div className="mdl-card__supporting-text">
          {description}
        </div>
        <div className="mdl-card__actions mdl-card--border">
          <Link to={`${QUIZZES_ROUTE}/${id}`} className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
            { SELECT_QUIZ_BUTTON }
          </Link>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  pic: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
};
export default Card;
