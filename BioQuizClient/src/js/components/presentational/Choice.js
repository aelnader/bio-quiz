import React from 'react';
import PropTypes from 'prop-types';
/**
 * Component for showing a choice for a question
 */
class Choice extends React.Component {
  constructor() {
    super();
    this.state = { isSelected: false };
    this.handleChoiceChange = this.handleChoiceChange.bind(this);
  }
  handleChoiceChange(changeEvent) {
    this.setState({
      isSelected: !this.state.isSelected,
    });
    this.props.onSelectChoice(changeEvent.target.value);
  }
  render() {
    const {
      choiceStr, questionIndex, index, showAnswer, isCorrect,
    } = this.props;
    let answerColor;
    if (showAnswer) {
      if (isCorrect) {
        answerColor = {
          color: '#00E676',
        };
      } else if (this.state.isSelected) {
        answerColor = {
          color: '#D50000',
        };
      }
    }
    const radioStyle = {
      display: 'inline',
    };
    return (
      <li className="mdl-list__item" style={answerColor}>
        <span className="mdl-list__item-primary-content choice-title">
          {choiceStr}
        </span>
        <span className="mdl-list__item-secondary-action" >
          <label className="demo-list-radio mdl-radio mdl-js-radio mdl-js-ripple-effect" htmlFor={`list-option-${questionIndex}-${index}`} style={radioStyle}>
            <input
              onChange={this.handleChoiceChange}
              type="radio"
              id={`list-option-${questionIndex}-${index}`}
              className="mdl-radio__button"
              name={`list-option-${questionIndex}`}
              value={index}
              disabled={showAnswer}
            />
          </label>
        </span>
      </li>
    );
  }
}
Choice.propTypes = {
  choiceStr: PropTypes.string.isRequired,
  questionIndex: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  onSelectChoice: PropTypes.func,
  showAnswer: PropTypes.bool,
  isCorrect: PropTypes.bool,
};
Choice.defaultProps = {
  onSelectChoice: () => {},
  showAnswer: false,
  isCorrect: false,
};
export default Choice;
