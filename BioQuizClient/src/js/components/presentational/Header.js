import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

/**
 * Header Component with links to each page
 * @param {string} title is the main title of the website
 * @param {array} pages array of page objects containing info about each page(id, name, link).
 */
const Header = ({ title, pages }) => {
  const tabs = pages.map(page =>
    (
      <Link to={page.route} className="mdl-navigation__link" href key={page.id}>
        {page.title}
      </Link>
    ));
  return (
    <header className="mdl-layout__header">
      <div className="mdl-layout__header-row">
        <span className="mdl-layout-title">{title}</span>
        <div className="mdl-layout-spacer" />
        <nav className="mdl-navigation mdl-layout--large-screen-only">
          {tabs}
        </nav>
      </div>
    </header>
  );
};
Header.propTypes = {
  title: PropTypes.string.isRequired,
  pages: PropTypes.array.isRequired,
};
export default Header;
