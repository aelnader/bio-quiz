import React from 'react';
import PropTypes from 'prop-types';

import Choice from './Choice';

/**
  * Component to show a question with multiple choices
  * @param {string} title                question title
  * @param {bool} isAnswered             did the user answer?
  * @param {number} correctAnswer        id for correct answer in choises list
  * @param {array} choices               list of possible choices
  */
class Question extends React.Component {
  constructor() {
    super();
    this.state = {};
    this.onSelectChoice = this.onSelectChoice.bind(this);
  }
  onSelectChoice(choiceIndex) {
    this.props.recordAnswer(this.props.questionIndex, parseInt(choiceIndex, 10) ===
    parseInt(this.props.correctAnswer, 10));
  }
  render() {
    const {
      title, choices, questionIndex, isAnswered, correctAnswer,
    } = this.props;
    const listStyle = {
      width: '700px',
    };
    const choicesElmts = choices.map((choiceStr, index) => (
      <Choice
        choiceStr={choiceStr}
        questionIndex={questionIndex}
        showAnswer={isAnswered}
        isCorrect={index === correctAnswer}
        index={index}
        onSelectChoice={this.onSelectChoice}
        key={choiceStr}
      />
    ));
    return (
      <div>
        <h3 className="question-title">{questionIndex + 1}) {title}</h3>
        <ul className="demo-list-control mdl-list" style={listStyle}>
          {choicesElmts}
        </ul>
      </div>
    );
  }
}
Question.propTypes = {
  title: PropTypes.string.isRequired,
  correctAnswer: PropTypes.number.isRequired,
  questionIndex: PropTypes.number.isRequired,
  isAnswered: PropTypes.bool.isRequired,
  choices: PropTypes.array.isRequired,
  recordAnswer: PropTypes.func,
};
Question.defaultProps = {
  recordAnswer: () => {},
};
export default Question;
