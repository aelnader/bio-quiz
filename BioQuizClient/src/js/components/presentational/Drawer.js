import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

/**
 * Drawer Component with links to each page on the left side of screen
 * @param {string} title is the main title of the website
 * @param {array} pages array of page objects containing info about each page(id, name, link).
 */
const Drawer = ({ title, pages }) => {
  const tabs = pages.map(page =>
    (
      <Link to={page.route} className="mdl-navigation__link" href key={page.id}>
        {page.title}
      </Link>
    ));
  return (
    <div className="mdl-layout__drawer">
      <span className="mdl-layout-title">{title}</span>
      <nav className="mdl-navigation">
        {tabs}
      </nav>
    </div>
  );
};

Drawer.propTypes = {
  title: PropTypes.string.isRequired,
  pages: PropTypes.array.isRequired,
};

export default Drawer;
