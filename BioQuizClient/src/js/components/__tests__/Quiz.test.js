import React from 'react';
import { shallow } from 'enzyme';

import { Quiz } from '../container/Quiz';
import Question from '../presentational/Question';

let wrapper;
let quiz;

beforeEach(() => {
  quiz = {
    id: 1,
    title: 'Quiz Title',
    description: 'Quiz description',
    img: '/images/url.jpg',
    questions: [
      {
        title: 'Question 1 :',
        correctAnswer: 0,
        choices: ['correct', 'wrong1', 'wrong2', 'wrong3'],
      },
      {
        title: 'Question 2',
        correctAnswer: 2,
        choices: ['wrong1', 'wrong1', 'correct', 'wrong3'],
      },
    ],
  };
  wrapper = shallow(<Quiz
    quiz={quiz}
  />);
});
describe('Quiz', () => {
  it('should show question title', () => {
    expect(wrapper.find('.quiz-title').length).toEqual(1);
  });
  it('should show all possible choices', () => {
    expect(wrapper.find(Question).length).toEqual(quiz.questions.length);
  });
});
