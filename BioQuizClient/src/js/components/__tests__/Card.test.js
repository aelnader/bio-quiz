import React from 'react';
import { shallow } from 'enzyme';

import Card from '../presentational/Card';

let quizz;
let wrapper;

beforeEach(() => {
  quizz = {
    id: 1,
    title: 'Reproduction conforme de l’ADN',
    description: 'Ceci est descriptif génial du test',
    img: '/images/adn.jpg',
    questions: [{}, {}],
  };
  wrapper = shallow(<Card
    title={quizz.title}
    description={quizz.description}
    pic={quizz.img}
    id={quizz.id}
  />);
});
describe('Card', () => {
  it('should show a title and price.', () => {
    expect(wrapper.contains(<h2 className="mdl-card__title-text">{quizz.title}</h2>)).toBeTruthy();
  });
  it('should show descriptions', () => {
    expect(wrapper.contains(<div className="mdl-card__supporting-text">{quizz.description}</div>)).toBeTruthy();
  });
  it('should show a link to add quizz to cart', () => {
    expect(wrapper.find('.mdl-button').length).toEqual(1);
  });
});
