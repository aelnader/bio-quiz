import React from 'react';
import { Link } from 'react-router-dom';
import { shallow } from 'enzyme';

import Header from '../presentational/Header';

let layoutConfig;
let wrapperHeader;

beforeEach(() => {
  // This is run before every test
  layoutConfig = {
    title: 'My Title for this page',
    pages: [
      {
        id: 'page1',
        title: 'page1',
        route: '/route1',
      },
      {
        id: 'page2',
        title: 'page2',
        route: '/route2',
      },
      {
        id: 'page3',
        title: 'page3',
        route: '/route3',
      },
    ],
  };
  wrapperHeader = shallow(<Header title={layoutConfig.title} pages={layoutConfig.pages} />);
});
describe('Header', () => {
  it('should have links to each page on the website', () => {
    expect(wrapperHeader.contains([<Link to={layoutConfig.pages[0].route} className="mdl-navigation__link" href key={layoutConfig.pages[0].id}>{layoutConfig.pages[0].title}</Link>,
      <Link to={layoutConfig.pages[1].route} className="mdl-navigation__link" href key={layoutConfig.pages[1].id}>{layoutConfig.pages[1].title}</Link>,
      <Link to={layoutConfig.pages[2].route} className="mdl-navigation__link" href key={layoutConfig.pages[2].id}>{layoutConfig.pages[2].title}</Link>])).toBeTruthy();
  });
});
