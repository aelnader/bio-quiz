import React from 'react';
import { shallow } from 'enzyme';

import { QuizzesShowcase } from '../container/QuizzesShowcase';
import Card from '../presentational/Card';
import { FETCH_QUIZZES_ERROR_MSG } from '../../constants/wordings';

let quizzes;
beforeEach(() => {
  quizzes = [
    {
      id: 1,
      title: 'Quiz Title',
      description: 'Quiz description',
      img: '/images/url.jpg',
      questions: [
        {
          title: 'Question 1 :',
          correctAnswer: 0,
          choices: ['correct', 'wrong1', 'wrong2', 'wrong3'],
        },
        {
          title: 'Question 2',
          correctAnswer: 2,
          choices: ['wrong1', 'wrong1', 'correct', 'wrong3'],
        },
      ],
    },
    {
      id: 2,
      title: 'Quiz Title 2',
      description: 'Quiz description 2',
      img: '/images/url2.jpg',
      questions: [
        {
          title: 'Question 3 :',
          correctAnswer: 0,
          choices: ['correct', 'wrong1', 'wrong2', 'wrong3'],
        },
        {
          title: 'Question 4',
          correctAnswer: 2,
          choices: ['wrong1', 'wrong1', 'correct', 'wrong3'],
        },
      ],
    },
  ];
});
describe('QuizzesShowcase', () => {
  it('should render a card for each quiz', () => {
    const error = {};
    const wrapper = shallow(<QuizzesShowcase quizzes={quizzes} error={error} />);
    expect(wrapper.find(Card)).toHaveLength(quizzes.length);
  });
  it('should show an error message if quizzes fetch failed', () => {
    const error = {
      code: 404,
      message: 'Page not found',
    };
    const wrapper = shallow(<QuizzesShowcase quizzes={[]} error={error} />);
    expect(wrapper.text()).toEqual(FETCH_QUIZZES_ERROR_MSG);
  });
});
