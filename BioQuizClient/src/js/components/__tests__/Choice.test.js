import React from 'react';
import { shallow } from 'enzyme';

import Choice from '../presentational/Choice';

let wrapper;

beforeEach(() => {
  wrapper = shallow(<Choice
    choiceStr="une reponse"
    questionIndex={1}
    index={1}
  />);
});
describe('Choice', () => {
  it('should show a radio button to select choice', () => {
    expect(wrapper.find('.choice-title').length).toEqual(1);
  });
  it('should show choice title', () => {
    expect(wrapper.find('input').length).toEqual(1);
  });
});
