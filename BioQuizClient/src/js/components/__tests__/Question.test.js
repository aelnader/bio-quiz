import React from 'react';
import { shallow } from 'enzyme';

import Question from '../presentational/Question';
import Choice from '../presentational/Choice';

let wrapper;
let choices;

beforeEach(() => {
  choices = ['choix1', 'choix2', 'choix3'];
  wrapper = shallow(<Question
    title="Titre de la question"
    questionIndex={1}
    isAnswered={false}
    correctAnswer={0}
    choices={choices}
  />);
});
describe('Question', () => {
  it('should show question title', () => {
    expect(wrapper.find('.question-title').length).toEqual(1);
  });
  it('should show all possible choices', () => {
    expect(wrapper.find(Choice).length).toEqual(choices.length);
  });
});
