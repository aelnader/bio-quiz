import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';
// actions
import { resultsSent } from '../../actions/results';
// components
import Question from '../presentational/Question';
// constants
import { VALIDATE_QUIZ_BUTTON, GO_BACK_QUIZ_BUTTON, QUIZ_SCORE_MSG } from '../../constants/wordings';
import { QUIZZES_ROUTE } from '../../constants/routes';
/**
 * Container/Page for showing user  a quiz
 */
export class Quiz extends React.Component {
  constructor(props) {
    super(props);
    const answers = [];
    for (let i = 0; i < props.quiz.questions.length; i += 1) {
      answers.push(false);
    }
    this.state = { isAnswered: false, answers };
    this.onValidateClick = this.onValidateClick.bind(this);
    this.recordAnswer = this.recordAnswer.bind(this);
  }
  onValidateClick() {
    this.setState({ isAnswered: true });
    const { quiz } = this.props;
    const { answers } = this.state;
    let score = 0;
    for (let i = 0; i < answers.length; i += 1) {
      if (answers[i]) {
        score += 1;
      }
    }
    this.props.dispatchResultsSent({
      quizId: quiz.id,
      nbrCorrect: score,
      nbrIncorrect: answers.length,
    });
  }
  recordAnswer(questionIndex, isAnsweredCorrectly) {
    const newAnswers = this.state.answers.slice();
    newAnswers[questionIndex] = isAnsweredCorrectly;
    this.setState({ answers: newAnswers });
  }
  render() {
    const { questions, title } = this.props.quiz;
    const { isAnswered, answers } = this.state;
    const buttonToShow = !isAnswered ?
      (
        <button onClick={this.onValidateClick} className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent button-valider">
          {VALIDATE_QUIZ_BUTTON}
        </button>
      ) :
      (
        <Link to={QUIZZES_ROUTE} className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent button-valider">
          {GO_BACK_QUIZ_BUTTON}
        </Link>
      );
    const questionElmts = questions.map((question, i) =>
      (
        <Question
          title={question.title}
          questionIndex={i}
          isAnswered={isAnswered}
          correctAnswer={question.correctAnswer}
          choices={question.choices}
          recordAnswer={this.recordAnswer}
          key={question.title}
        />
      ));
    let score = 0;
    for (let i = 0; i < answers.length; i += 1) {
      if (answers[i]) {
        score += 1;
      }
    }
    const scoreMsg = isAnswered ? (<span>{QUIZ_SCORE_MSG}{score}/{answers.length}</span>) : null;
    return (
      <div>
        <h2 className="quiz-title">{title}</h2>
        <div>{questionElmts}</div>
        {buttonToShow} {scoreMsg}
      </div>
    );
  }
}

Quiz.propTypes = {
  quiz: PropTypes.object,
  dispatchResultsSent: PropTypes.func,
};
Quiz.defaultProps = {
  quiz: {},
  dispatchResultsSent: () => {},
};
function mapStateToProps(state, ownProps) {
  const { quizId } = ownProps.match.params;
  const quiz = state.quizzes.quizzesList.find(element => element.id === parseInt(quizId, 10));
  const mappedProps = {
    quiz,
  };
  return mappedProps;
}
function mapDispatchToProps(dispatch) {
  return ({
    dispatchResultsSent: (payload) => { dispatch(resultsSent(payload)); },
  });
}
export default connect(mapStateToProps, mapDispatchToProps)(Quiz);
