import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';
// components
import Card from '../presentational/Card';
// constants
import { FETCH_QUIZZES_ERROR_MSG } from '../../constants/wordings';
/**
 * Container/Page for showing quizzes
 * @param {array} quizzes quizzes list to show, each quiz has attr: id, title,
 * description, img, questions.
 * @param {object} error the error in fetching quizzes, has attr: code, msg
 */
export class QuizzesShowcase extends React.Component {
  constructor() {
    super();
    this.state = {};
  }
  render() {
    const { error, quizzes } = this.props;
    if (quizzes.length === 0) {
      if (error.code) {
        // fetching quizzes failed
        return (<h1>{FETCH_QUIZZES_ERROR_MSG}</h1>);
      }
      // fetching quizzes in progress
      return (<h1>Loading</h1>);
    }
    const quizzcards = quizzes.map(quiz =>
      (
        <Card
          title={quiz.title}
          description={quiz.description}
          pic={quiz.img}
          key={quiz.id}
          id={quiz.id}
          onQuizSelect={() => {}}
        />
      ));
    return (
      <div>
        <div className="cards-holder">{ quizzcards }</div>
      </div>
    );
  }
}
QuizzesShowcase.propTypes = {
  quizzes: PropTypes.array,
  error: PropTypes.object,
};
QuizzesShowcase.defaultProps = {
  quizzes: [],
  error: {},
};
function mapStateToProps(state) {
  const mappedProps = {
    quizzes: state.quizzes.quizzesList,
    error: state.quizzes.error,
  };
  return mappedProps;
}

export default connect(mapStateToProps)(QuizzesShowcase);
