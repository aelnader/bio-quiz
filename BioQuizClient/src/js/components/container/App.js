import { connect } from 'react-redux';
import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
// action
import { quizzesRequested } from '../../actions/quizzes';
// components
import Layout from './Layout';
// constants
import { MAIN_TITLE, PAGE_QUIZZES_TITLE, PAGE_RESULTS_TITLE } from '../../constants/wordings';
import { QUIZZES_ROUTE, RESULTS_ROUTE } from '../../constants/routes';
/**
 * Main component, everything renders inside App
 */
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      layoutConfig: {
        title: MAIN_TITLE,
        pages: [
          {
            id: `${PAGE_QUIZZES_TITLE}${1}`,
            title: PAGE_QUIZZES_TITLE,
            route: QUIZZES_ROUTE,
          },
          {
            id: `${PAGE_RESULTS_TITLE}${2}`,
            title: PAGE_RESULTS_TITLE,
            route: RESULTS_ROUTE,
          },
        ],
      },
    };
  }
  componentDidMount() {
    this.props.dispatchQuizzessRequested();
  }
  render() {
    const { layoutConfig } = this.state;
    return (
      <div>
        <Layout title={layoutConfig.title} pages={layoutConfig.pages} />
      </div>
    );
  }
}
App.propTypes = {
  dispatchQuizzessRequested: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return ({
    dispatchQuizzessRequested: () => { dispatch(quizzesRequested()); },
  });
}

export default withRouter(connect(null, mapDispatchToProps)(App));
