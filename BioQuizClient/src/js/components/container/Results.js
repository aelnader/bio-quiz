import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';

// actions
import { resultsRequested } from '../../actions/results';
/**
 * Container/Page for showing user quiz results
 */
export class Results extends React.Component {
  constructor() {
    super();
    this.state = {};
  }
  componentDidMount() {
    this.props.dispatchResultsRequested();
  }
  render() {
    const { results, quizzes } = this.props;
    const resultsRows = results.map((result, index) => {
      const quiz = quizzes.find(element => (element.id) === result.quizId);
      if (quiz) {
        return (
          <tr key={`${quiz.title}${index}`} className="item-detail-row">{// eslint-disable-line
          }<td className="mdl-data-table__cell--non-numeric">{quiz.title}</td>
            <td>{result.nbrCorrect} / {result.nbrIncorrect}</td>
          </tr>
        );
      }
      return null;
    });
    return (
      <table className="mdl-data-table mdl-js-data-table mdl-shadow--2dp cart-table">
        <thead>
          <tr>
            <th className="mdl-data-table__cell--non-numeric">Quiz</th>
            <th>Résultat</th>
          </tr>
        </thead>
        <tbody>
          {resultsRows}
        </tbody>
      </table>
    );
  }
}
Results.propTypes = {
  dispatchResultsRequested: PropTypes.func,
  results: PropTypes.array,
  quizzes: PropTypes.array,
};
Results.defaultProps = {
  dispatchResultsRequested: () => {},
  results: [],
  quizzes: [],
};
function mapStateToProps(state) {
  const mappedProps = {
    results: state.results.resultsList,
    quizzes: state.quizzes.quizzesList,
  };
  return mappedProps;
}
function mapDispatchToProps(dispatch) {
  return ({
    dispatchResultsRequested: () => { dispatch(resultsRequested()); },
  });
}
export default connect(mapStateToProps, mapDispatchToProps)(Results);
