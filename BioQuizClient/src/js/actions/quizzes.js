import { QUIZZES_REQUESTED, QUIZZES_SUCCEEDED, QUIZZES_FAILED } from '../constants/actionTypes';

/**
 * All actions related to quizzes
 */
export function quizzesRequested() {
  return {
    type: QUIZZES_REQUESTED,
  };
}
export function quizzesSucceeded(payload) {
  return {
    type: QUIZZES_SUCCEEDED,
    payload,
  };
}
export function quizzesFailed(payload) {
  return {
    type: QUIZZES_FAILED,
    payload,
    error: true,
  };
}
