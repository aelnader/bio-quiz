import { RESULTS_REQUESTED, RESULTS_SUCCEEDED, RESULTS_FAILED, RESULTS_SENT } from '../constants/actionTypes';

/**
 * All actions related to results
 */
export function resultsRequested() {
  return {
    type: RESULTS_REQUESTED,
  };
}
export function resultsSucceeded(payload) {
  return {
    type: RESULTS_SUCCEEDED,
    payload,
  };
}
export function resultsFailed(payload) {
  return {
    type: RESULTS_FAILED,
    payload,
    error: true,
  };
}
export function resultsSent(payload) {
  return {
    type: RESULTS_SENT,
    payload,
  };
}
