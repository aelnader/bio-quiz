import * as actions from '../quizzes';
import * as types from '../../constants/actionTypes';

describe('quizzes actions', () => {
  it('should create an action to request quizzes', () => {
    const expectedAction = {
      type: types.QUIZZES_REQUESTED,
    };
    expect(actions.quizzesRequested()).toEqual(expectedAction);
  });
  it('should create an action to signal a succesful quizzes fetch', () => {
    const payload =
      [
        {
          id: 1,
          title: 'Reproduction conforme de l’ADN',
          description: 'Ceci est descriptif génial du test',
          img: '/images/adn.jpg',
          questions: [{}, {}],
        },
      ];
    const expectedAction = {
      type: types.QUIZZES_SUCCEEDED,
      payload,
    };
    expect(actions.quizzesSucceeded(payload)).toEqual(expectedAction);
  });
  it('should create an action to signal a failed quizzes fetch', () => {
    const payload = {
      code: '404',
      message: 'Page not found',
    };
    const expectedAction = {
      type: types.QUIZZES_FAILED,
      payload,
      error: true,
    };
    expect(actions.quizzesFailed(payload)).toEqual(expectedAction);
  });
});
