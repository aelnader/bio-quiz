import { call, put } from 'redux-saga/effects';
// actions
import { quizzesSucceeded, quizzesFailed } from '../actions/quizzes';
// constants
import { GET_QUIZZES_URL } from '../constants/api';

export function fetchQuizzes() {
  return fetch(GET_QUIZZES_URL).then(response => response.json());
}
/**
 * Handles quizzes data fetch and synchronization with the application
 */
export function* quizzesSaga() {
  try {
    const quizzes = yield call(fetchQuizzes);
    yield put(quizzesSucceeded(quizzes));
  } catch (e) {
    yield put(quizzesFailed(e));
  }
}
