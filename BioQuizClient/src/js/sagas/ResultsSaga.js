import { call, put, fork } from 'redux-saga/effects';
// constants
import { RESULTS_URL } from '../constants/api';
// actions
import { resultsSucceeded, resultsFailed } from '../actions/results';

export function fetchResults() {
  return fetch(RESULTS_URL).then(response => response.json());
}

export function sendResults(payload) {
  fetch(
    RESULTS_URL,
    {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(payload),
    },
  ).then(response => response.json());
}
/**
 * Handles sending results
 */
export function* sendResultsSaga(action) {
  try {
    yield fork(sendResults, action.payload);
  } catch (e) {
    // Do nothing now
  }
}

/**
 * Handles results data fetch and synchronization with the application
 */
export function* resultsSaga() {
  try {
    const results = yield call(fetchResults);
    yield put(resultsSucceeded(results));
  } catch (e) {
    yield put(resultsFailed(e));
  }
}
