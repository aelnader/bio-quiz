import { takeLatest } from 'redux-saga/effects';
// sagas
import { quizzesSaga } from './QuizzesSaga';
import { sendResultsSaga, resultsSaga } from './ResultsSaga';
// constants
import { QUIZZES_REQUESTED, RESULTS_SENT, RESULTS_REQUESTED } from '../constants/actionTypes';
/**
 * Main saga which runs all sagas handling the application's side effects
 */
export default function* mainSaga() {
  // Initiate sagas here
  yield takeLatest(QUIZZES_REQUESTED, quizzesSaga);
  yield takeLatest(RESULTS_SENT, sendResultsSaga);
  yield takeLatest(RESULTS_REQUESTED, resultsSaga);
}
