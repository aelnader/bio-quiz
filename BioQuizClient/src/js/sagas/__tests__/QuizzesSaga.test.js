import { call, put } from 'redux-saga/effects';
import { cloneableGenerator } from 'redux-saga/utils';

import { quizzesSaga, fetchQuizzes } from '../QuizzesSaga';
import { quizzesSucceeded, quizzesFailed } from '../../actions/quizzes';

describe('QuizzesSaga', () => {
  const quizzesSagaGen = cloneableGenerator(quizzesSaga)();
  it('should try to fetch the quizzes data', () => {
    expect(quizzesSagaGen.next().value).toEqual(call(fetchQuizzes));
  });
  it('should call an action to signal success and store data in case off success', () => {
    const quizzesSagaGenClone = quizzesSagaGen.clone();
    const quizzesResult = [
      {
        id: 1,
        title: 'Reproduction conforme de l’ADN',
        description: 'Ceci est descriptif génial du quiz',
        img: '/images/adn.jpg',
        questions: [{}, {}],
      },
    ];
    expect(quizzesSagaGenClone.next(quizzesResult).value)
      .toEqual(put(quizzesSucceeded(quizzesResult)));
  });
  it('should call an action to signal error in case of error', () => {
    const quizzesSagaGenClone = quizzesSagaGen.clone();
    const error = {
      code: '404',
      message: 'Page not found',
    };
    expect(quizzesSagaGenClone.throw(error).value)
      .toEqual(put(quizzesFailed(error)));
  });
});
