import { takeLatest } from 'redux-saga/effects';
import mainSaga from '../MainSaga';
import { quizzesSaga } from '../QuizzesSaga';
import * as actionTypes from '../../constants/actionTypes';

describe('MainSaga', () => {
  const mainSagaGen = mainSaga();
  it('should run the quizzesSaga each time QUIZZES_REQUESTED action is dispatched', () => {
    expect(mainSagaGen.next().value)
      .toEqual(takeLatest(actionTypes.QUIZZES_REQUESTED, quizzesSaga));
  });
});
