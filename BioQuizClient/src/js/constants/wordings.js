/**
 * This file contains string constants for all wordings(msgs, labels, titles etc.)
 */

// Layout
export const MAIN_TITLE = 'Bio Quiz';
export const PAGE_QUIZZES_TITLE = 'Quizzes';
export const PAGE_RESULTS_TITLE = 'Mes Résultats';

// Labels
export const SELECT_QUIZ_BUTTON = 'Passer le quiz';
export const VALIDATE_QUIZ_BUTTON = 'Valider les réponses';
export const GO_BACK_QUIZ_BUTTON = 'Passer un autre quiz!';
export const QUIZ_SCORE_MSG = 'Votre score est :';
// erros msgs
export const FETCH_QUIZZES_ERROR_MSG = 'Nous ne pouvons pas afficher les quiz pour le moment, merci de revenir plus tard';

// msgs
export const RESULTS_EMPTY_MSG = 'Vous n\'avez effectuez aucun test pour le moment';
