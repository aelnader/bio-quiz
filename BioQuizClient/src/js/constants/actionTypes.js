/**
 * This file contains string constants for action types
 */

// quizzes
export const QUIZZES_REQUESTED = 'QUIZZES_REQUESTED';
export const QUIZZES_SUCCEEDED = 'QUIZZES_SUCCEEDED';
export const QUIZZES_FAILED = 'QUIZZES_FAILED';

// results
export const RESULTS_REQUESTED = 'RESULTS_REQUESTED';
export const RESULTS_SUCCEEDED = 'RESULTS_SUCCEEDED';
export const RESULTS_FAILED = 'RESULTS_FAILED';
export const RESULTS_SENT = 'RESULTS_SENT';
