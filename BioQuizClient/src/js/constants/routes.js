/**
 * This file contains string constants for url routes
 */
export const QUIZZES_ROUTE = '/quizzes';
export const RESULTS_ROUTE = '/results';
