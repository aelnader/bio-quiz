/**
 * This file contains string constants for api calls
 */
export const BIO_QUIZ_API_BASE_URL = '/api';
// quizzes
export const GET_QUIZZES_URL = `${BIO_QUIZ_API_BASE_URL}/quizzes`;
// results
export const RESULTS_URL = `${BIO_QUIZ_API_BASE_URL}/results`;
