
# Bio Quiz

This is a website to do some biology quizzes.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

If you don't have yarn installed on your machine:

```
npm install -g yarn
```
If you don't have gulp installed on your machine:

```
npm install -g gulp
```

### Installing

#### Server

Install npm dependencies for server. In ./BioQuizServer run:

```
yarn install
```

To run the server on your local machine:

```
gulp
```
By default, node server will run on http://localhost:8081. (You can change ports in gulpfile.js)

A default client is served, so you should be able to see what the project looks like if you visit the link at this point.

#### Client

To run a copy of the client on your machine, begin by installing dependencies for the client. In ./BioQuizClient run:
```
yarn install
```
To run webpack-dev-server on your local machine:

```
yarn start
```
By default, webpack-dev-server will run on http://localhost:8080.

All api calls are redirected to port 8081. If you have your server running on another on another port, make sure to change the proxy value in webpack.dev.config.

## Running the tests

To run unit tests for server, in ./BioQuizServer run:

```
yarn test
```

To run unit tests for client, in ./BioQuizClient run:

```
yarn test
```

## Deployment

To build the JS client bundle, in ./BioQuizClient run:
```
yarn build
```
This generate a bundle in ./dist folder ready to be deployed on server.

## Built With
* [Node.js](https://nodejs.org/en/) - JavaScript on server
* [Express](https://expressjs.com/) - Framework for Node.js
* [React.js](https://reactjs.org/) - The web framework used
* [Redux](https://redux.js.org/) - State Management
* [Redux Saga](https://github.com/redux-saga/redux-saga) - To manage side-effects
* [Jest](https://facebook.github.io/jest/) - Testing
* [Enzyme](https://github.com/airbnb/enzyme) - Testing utilities for React
* [Google's MDL](https://getmdl.io/) - CSS Framework
## Authors

* **Alaa EL NADERI**

## License

This project is licensed under the MIT License
