var resultsModel;
var status;
var send;

beforeEach(function() {
  resultsModel = [];
  status = jest.fn();
  send = jest.fn();
});
afterEach(function() {
  status.mockReset();
});
describe('Result Controller Tests:', function(){
      describe('Post', function(){
        it('should have integer value for field quizId', function(){
          var req = {
            body: {
              quizId: '1',
              nbrCorrect: 2,
              nbrIncorrect: 1
            }
          };
          var res = {
            status: status,
            send: send,
          };
          var resultController = require('./resultController')(resultsModel);
          resultController.post(req,res);
          // Check that the function has been called
          expect(status.mock.calls.length).toEqual(1);
          // Check it's been called with the right arguments
          expect(status.mock.calls[0][0]).toEqual(400);
        });
        it('should have integer value for field nbrCorrect', function(){
          var req = {
            body: {
              quizId: 1,
              nbrCorrect: '2',
              nbrIncorrect: 1
            }
          };
          var res = {
            status: status,
            send: send,
          };
          var resultController = require('./resultController')(resultsModel);
          resultController.post(req,res);
          // Check that the function has been called
          expect(status.mock.calls.length).toEqual(1);
          // Check it's been called with the right arguments
          expect(status.mock.calls[0][0]).toEqual(400);
        });
        it('should have integer value for field nbrIncorrect', function(){
          var req = {
            body: {
              quizId: 1,
              nbrCorrect: 2,
            }
          };
          var res = {
            status: status,
            send: send,
          };
          var resultController = require('./resultController')(resultsModel);
          resultController.post(req,res);
          // Check that the function has been called
          expect(status.mock.calls.length).toEqual(1);
          // Check it's been called with the right arguments
          expect(status.mock.calls[0][0]).toEqual(400);
        });
        it('should return Ok if all fields are correct', function(){
          var req = {
            body: {
              quizId: 1,
              nbrCorrect: 2,
              nbrIncorrect: 1
            }
          };
          var res = {
            status: status,
            send: send,
          };
          var resultController = require('./resultController')(resultsModel);
          resultController.post(req,res);
          // Check that the function has been called
          expect(status.mock.calls.length).toEqual(1);
          // Check it's been called with the right arguments
          expect(status.mock.calls[0][0]).toEqual(201);
        });
      });
    });
