var express = require('express');

var routes = function (resultModel) {
  var resultRouter = express.Router(); // eslint-disable-line
  var resultController = require('./resultController')(resultModel);

  resultRouter.route('/')
    .get(resultController.get)
    .post(resultController.post);
  return resultRouter;
};
module.exports = routes;
