var resultController = function (resultModel) {
  var get = function (req, res) {
    res.json(resultModel);
  };
  var post = function(req, res){
      var result = req.body;

      if (!Number.isInteger(result.quizId)
        || !Number.isInteger(result.nbrCorrect)
        || !Number.isInteger(result.nbrIncorrect)) {
          res.status(400);
          res.send('Insert ambiguous bad request message here');
      } else {
          resultModel.push(result);
          res.status(201);
          res.send(result);
      }
  };
  return {
    get: get,
    post: post,
  };
};
module.exports = resultController;
