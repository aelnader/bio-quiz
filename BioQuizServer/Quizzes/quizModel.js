var quizzez = [
  {
    id: 1,
    title: 'Reproduction conforme de l’ADN',
    description: 'Ceci est descriptif du test reproduction conforme de l’ADN',
    img: '/images/adn.jpg',
    questions: [
      {
        title: 'Les chromosomes :',
        correctAnswer: 0,
        choices: ['possèdent deux chromatides en phase G1',
                        'sont d\'avantage condensés en mitose qu’en interphase',
                        'possèdent une chromatide en métaphase',
                        'ne sont présents que lors de la mitose'],
      },
      {
        title: 'La mitose :',
        correctAnswer: 2,
        choices: ['correspond à la totalité du cycle cellulaire',
          'est un processus précédé par une réplication conservative de l’ADN',
          'permet de transmettre la totalité de l’information'
          +'génétique d’une cellule mère à 2 cellules filles',
          'ne nécessite pas d’énergie'],
      },
      {
        title: 'La prophase est la phase :',
        correctAnswer: 1,
        choices: ['de séparation des cellules filles',
                        'pendant laquelle l’ADN se condense en structures '+
                        'individualisées, les chromosomes',
                        'au cours de laquelle les chromosomes sont à une'+
                        'chromatide',
                        'au cours de laquelle les chromatides de chaque '+
                        'chromosome se séparent et migrent vers les pôles '+
                        'opposés de la cellule'],
      },
    ],
  },
  {
    id: 2,
    title: 'Variabilité génétique et mutation de l’ADN',
    description: 'Ceci est descriptif génial du test du test sur la variabilité'
    +' génétique et mutation de l’ADN',
    img: '/images/Gene-Mutation.jpg',
    questions: [
      {
        title: 'La transcription de l\'ADN des Eucaryotes :',
        correctAnswer: 1,
        choices: ['a lieu dans le cytoplasme',
                  'donne naissance à un ARN pré-messager',
                  'donne naissance à une protéine',
                'consiste à copier l\'un des deux brins de la molécule d\'ADN'],
      },
      {
        title: 'Un ARN messager :',
        correctAnswer: 3,
        choices: ['porte l\'intégralité de l\'information d\'une molécule'
        + ' d\'ADN',
                  'résulte de la traduction d\'une séquence d\'ADN',
                  'est constitué des mêmes nucléotides que l\'ADN',
                  'est traduit en protéines'],
      },
      {
        title: 'Le code génétique :',
        correctAnswer: 3,
        choices: ['correspond à l\'information génétique d\'un individu',
                  'diffère d\'un individu à l\'autre',
                  'est le système de correspondance entre ADN et ARN '+
                  'messager',
                  'est le système de correspondance mis en jeu lors de '+
                  'la traduction de l\'information '],
      },
    ],
  },
  {
    id: 4,
    title: 'Patrimoine génétique et maladie',
    description: 'Ceci est descriptif du test Patrimoine génétique et maladie',
    img: '/images/gene-maladie.jpg',
    questions: [
      {
        title: 'Les maladies cardio-vasculaires :',
        correctAnswer: 2,
        choices: ['sont systématiquement déclenchées par la présence d’allèles'+
        ' mutés dans le génome des malades',
                  'n’apparaissent jamais si l’on adopte un comportement adapté',
                  'une personne présentant un risque génétique peut le minorer'+
                  ' en adoptant un mode de vie adapté',
                  'l’absence de gènes de prédisposition et une activité'+
                  ' sportive régulière sont une garantie contre le risque de'+
                  'ces maladies'],
      },
      {
        title: 'La mucoviscidose :',
        correctAnswer: 3,
        choices: ['est une maladie multifactorielle fréquente chez les'+
        ' individus présentant un allèle de prédispositionre',
                  'est une maladie génétique transmise selon un mode récessif'+
                  ' et qui provoque une atrophie du muscle',
                  'peut être traitée par un régime alimentaire adapté',
                  'est due à la déficience d’un gène codant la fabrication '+
                  'd’un canal transmembranaire aux ions chlorures'],
      },
    ],
  },
];
module.exports = quizzez;
