var quizController = function (quizModel) {
  var get = function (req, res) {
    res.json(quizModel);
  };
  return {
    get: get,
  };
};
module.exports = quizController;
