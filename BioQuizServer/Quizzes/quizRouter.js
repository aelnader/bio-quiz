var express = require('express');

var routes = function (quizModel) {
  var quizRouter = express.Router(); // eslint-disable-line
  var quizController = require('./quizController')(quizModel);

  quizRouter.route('/')
    .get(quizController.get);
  return quizRouter;
};
module.exports = routes;
