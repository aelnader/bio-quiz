var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var quizzes = require('./Quizzes/quizModel');
var results = require('./Results/resultModel');

var app = express();

var port = process.env.PORT || 3000;

app.use(express.static('public'));

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

var quizRouter = require('./Quizzes/quizRouter')(quizzes);
var resultRouter = require('./Results/resultRouter')(results);

app.use('/api/quizzes', quizRouter);
app.use('/api/results', resultRouter);

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

app.listen(port, function(){
	console.log('Listening on port ' + port);
});
