var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    eslint = require('gulp-eslint');

gulp.task('lint', function(){
      return gulp.src(['**/*.js','!node_modules/**', '!**/*bundle.js*'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
  });
gulp.task('default', ['lint'], function(){
    nodemon({
      script: 'app.js',
      ext: 'js',
      env: {
        PORT:8081
      },
      ignore: ['./node_modules/**', '!**/*bundle.js*']
    })
    .on('restart', ['lint'], function(){
      console.log('Restarting');
    });
});
